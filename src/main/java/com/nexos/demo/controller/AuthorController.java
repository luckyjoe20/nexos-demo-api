package com.nexos.demo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nexos.demo.dto.AuthorDTO;
import com.nexos.demo.service.LibraryService;

@RestController
@RequestMapping(value = "/api")
public class AuthorController {

	@Autowired
	LibraryService libraryService;

	@GetMapping(value = "/author", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<AuthorDTO>> getAllAuthor() {

		List<AuthorDTO> authorList = new ArrayList<AuthorDTO>();

		authorList = libraryService.getAllAuthors();

		return new ResponseEntity<>(authorList, HttpStatus.OK);

	}

	@PostMapping(value = "/author", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> createAuthor(@Valid @RequestBody AuthorDTO authorRequest) {

		libraryService.createAuthor(authorRequest);

		return new ResponseEntity<>(HttpStatus.OK);

	}
	
	
	@GetMapping(value = "/author/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<AuthorDTO>> getAuthorsByName(@Valid @PathVariable("name") String name) {

		return new ResponseEntity<>(libraryService.getAuthorByName(name), HttpStatus.OK);

	}
	

}


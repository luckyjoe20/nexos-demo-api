package com.nexos.demo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nexos.demo.dto.PublisherDTO;
import com.nexos.demo.service.LibraryService;

@RestController
@RequestMapping(value = "/api")
public class PublisherController {

	@Autowired
	LibraryService libraryService;

	@GetMapping(value = "/publisher", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<PublisherDTO>> getAllPublisher() {

		List<PublisherDTO> publisherList = new ArrayList<PublisherDTO>();

		publisherList = libraryService.getAllPublishers();

		return new ResponseEntity<>(publisherList, HttpStatus.OK);

	}

	@PostMapping(value = "/publisher", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> createPublisher(@Valid @RequestBody PublisherDTO publisherRequest) {

		libraryService.createPublisher(publisherRequest);

		return new ResponseEntity<>(HttpStatus.OK);

	}
	
	
	@GetMapping(value = "/publisher/{name}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<PublisherDTO>> getPublishersByName(@Valid @PathVariable("name") String name) {

		return new ResponseEntity<>(libraryService.getPublisherByName(name), HttpStatus.OK);

	}
	

}

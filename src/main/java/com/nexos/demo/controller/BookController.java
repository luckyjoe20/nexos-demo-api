package com.nexos.demo.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nexos.demo.dto.BookDTO;
import com.nexos.demo.service.LibraryService;

@CrossOrigin
@RestController
@RequestMapping(value = "/api")
public class BookController {

	@Autowired
	LibraryService libraryService;

	@GetMapping(value = "/book", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<BookDTO>> getAllBook() {

		List<BookDTO> bookList = new ArrayList<BookDTO>();

		bookList = libraryService.getAllBooks();


		return new ResponseEntity<>(bookList, HttpStatus.OK);

	}

	@PostMapping(value = "/book", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> createBook(@Valid @RequestBody BookDTO BookRequest) {

		libraryService.createBook(BookRequest);

		return new ResponseEntity<>(HttpStatus.CREATED);

	}
	
	
	@GetMapping(value = "/book/{title}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<List<BookDTO>> getBooksByTitle(@Valid @PathVariable("title") String title) {

		return new ResponseEntity<>(libraryService.getBookByTitle(title), HttpStatus.OK);

	}
	

}

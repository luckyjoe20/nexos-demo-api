package com.nexos.demo.utility;

import java.util.List;
import java.util.stream.Collectors;

import com.nexos.demo.dto.AuthorDTO;
import com.nexos.demo.dto.BookDTO;
import com.nexos.demo.dto.PublisherDTO;
import com.nexos.demo.model.Author;
import com.nexos.demo.model.Book;
import com.nexos.demo.model.Publisher;

public class DTOConversionUtil {

	public Publisher convertDTOToPublisher(PublisherDTO publisherRequest) {
		
		Publisher publisher = new Publisher();

		publisher.setName(publisherRequest.getName());
		publisher.setMailDirection(publisherRequest.getMailDirection());
		publisher.setPhoneNumber(publisherRequest.getPhoneNumber());
		publisher.setEmail(publisherRequest.getEmail());
		publisher.setMaxBookCount(publisherRequest.getMaxBookCount());

		return publisher;
	}

	public Book convertDTOToBook(BookDTO bookRequest) {

		Book book = new Book();

		book.setTitle(bookRequest.getTitle());
		book.setYear(bookRequest.getYear());
		book.setGender(bookRequest.getGender());
		book.setPageCount(bookRequest.getPageCount());
		book.setPublisher(null);
		book.setAuthor(null);

		return book;
	}

	public Author convertDTOToAuthor(AuthorDTO authorRequest) {

		Author author = new Author();

		author.setName(authorRequest.getName());
		author.setBirthDate(authorRequest.getBirthDate());
		author.setCityOfOrigin(authorRequest.getCityOfOrigin());
		author.setEmail(authorRequest.getEmail());

		return author;
	}
	
	public List<AuthorDTO> convertAuthorsToListDTO(List<Author> authorListResponse) {

		List<AuthorDTO> listDTO = authorListResponse
									.stream()
							        .map(author -> new AuthorDTO(author.getName(), 
							        							 author.getBirthDate(),
							        							 author.getCityOfOrigin(),
							        							 author.getEmail()))
							        .collect(Collectors.toList());

		return listDTO;
	}
	
	public List<BookDTO> convertBooksToListDTO(List<Book> bookListResponse) {

		List<BookDTO> listDTO = bookListResponse
									.stream()
							        .map(book -> new BookDTO(book.getTitle(), 
							        		book.getYear(), 
							        		book.getGender(),
							        		book.getPageCount(), 
							        		book.getPublisher().getName(),
							        		book.getAuthor().getName()))
							        .collect(Collectors.toList());

		return listDTO;
	}
	
	public List<PublisherDTO> convertPublishersToListDTO(List<Publisher> publisherListResponse) {

		List<PublisherDTO> listDTO = publisherListResponse
									.stream()
							        .map(publisher -> new PublisherDTO(publisher.getName(), 
						        								publisher.getMailDirection(),
							        							publisher.getPhoneNumber(),
							        							publisher.getEmail(),
							        							publisher.getMaxBookCount()))
							        .collect(Collectors.toList());

		return listDTO;
	}

}

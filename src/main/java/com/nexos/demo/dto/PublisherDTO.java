package com.nexos.demo.dto;

import java.io.Serializable;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class PublisherDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7521625034361152186L;
	@JsonProperty("name")
	@NotBlank(message = "El nombre de la editorial es obligatorio")
	private String name;
	@JsonProperty("mail_direction")
	@NotBlank(message = "La direccion de correspondencia es obligatoria")
	private String mailDirection;
	@JsonProperty("phone_number")
	@Pattern(regexp="(^$|[0-9]{10})",message="El número de telefono debe ser valido")
	private String phoneNumber;
	@JsonProperty("email")
	@Email(message = "El campo correo debe contener un email válido")
	private String email;
	@JsonProperty("max_book_count")
	private Integer maxBookCount;

}

package com.nexos.demo.dto;

import java.io.Serializable;
import java.time.LocalDate;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AuthorDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 8906010637773759276L;
	@JsonProperty("name")
	@NotBlank(message = "El nombre del autor es obligatorio")
	private String name;
	@JsonProperty("birth_date")
	private LocalDate birthDate;
	@JsonProperty("city")
	private String cityOfOrigin;
	@JsonProperty("email")
	private String email;

}

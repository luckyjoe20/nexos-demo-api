package com.nexos.demo.dto;

import java.io.Serializable;
import java.time.LocalDate;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class BookDTO implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 9032605852248770074L;
	@NotBlank(message = "El titulo del libro es obligatorio")
	private String title;
	private LocalDate year;
	@NotBlank(message = "El genero es obligatorio")
	private String gender;
	@JsonProperty("page_count")
	@Positive(message="la cantidad de paginas debe ser mayor a 0")
	private Integer pageCount;
	@NotBlank(message = "El nombre de la editorial es obligatorio")
	private String publisher;
	@NotBlank(message = "El nombre del autor es obligatorio")
	private String author;


}

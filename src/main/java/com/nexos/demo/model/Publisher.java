package com.nexos.demo.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection="publisher")
public class Publisher {
	
	@Id
	private ObjectId _id;
	@Field("name")
	private String name;
	@Field("mail_direction")
	private String mailDirection;
	@Field("phone_number")
	private String phoneNumber;
	@Field("email")
	private String email;
	@Field("max_book_count")
	private Integer maxBookCount;
	@Field("book_count")
	private Integer bookCount;
	
}

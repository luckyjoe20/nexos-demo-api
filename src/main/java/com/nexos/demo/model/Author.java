package com.nexos.demo.model;

import java.time.LocalDate;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection="author")
public class Author {
	
	@Id
	private ObjectId _id;
	private String name;
	@Field("birth_date")
	private LocalDate birthDate;
	@Field("city_of_origin")
	private String cityOfOrigin;
	private String email;

}

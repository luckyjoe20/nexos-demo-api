package com.nexos.demo.model;

import java.time.LocalDate;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Document(collection="book")
public class Book {

	@Id
	private ObjectId _id;
	private String title;
	private LocalDate year;
	private String gender;
	@Field("page_count")
	private Integer pageCount;
	private Publisher publisher;
	private Author author;

}

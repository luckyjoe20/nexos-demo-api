package com.nexos.demo.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.nexos.demo.model.Book;

public interface BookRepository extends MongoRepository<Book, String>{
	List<Book> findByTitleContaining(String title);
}

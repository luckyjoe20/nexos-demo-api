package com.nexos.demo.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.nexos.demo.model.Publisher;

public interface PublisherRepository extends MongoRepository<Publisher, String>{
	List<Publisher> findByNameContaining(String name);
	Publisher findByName(String name);
}

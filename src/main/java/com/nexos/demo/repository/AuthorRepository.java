package com.nexos.demo.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.nexos.demo.model.Author;

public interface AuthorRepository extends MongoRepository<Author, String>{
	List<Author> findByNameContaining(String name);
	Author findByName(String name);
}

package com.nexos.demo.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nexos.demo.dto.AuthorDTO;
import com.nexos.demo.dto.BookDTO;
import com.nexos.demo.dto.PublisherDTO;
import com.nexos.demo.exception.FailedOperationException;
import com.nexos.demo.exception.NoDataFoundException;
import com.nexos.demo.model.Author;
import com.nexos.demo.model.Book;
import com.nexos.demo.model.Publisher;
import com.nexos.demo.repository.AuthorRepository;
import com.nexos.demo.repository.BookRepository;
import com.nexos.demo.repository.PublisherRepository;
import com.nexos.demo.service.LibraryService;
import com.nexos.demo.utility.DTOConversionUtil;

@Service
public class LibraryServiceImpl extends DTOConversionUtil implements LibraryService {
	
	@Autowired
	PublisherRepository publisherRepository;
	
	@Autowired
	AuthorRepository authorRepository;
	
	@Autowired
	BookRepository bookRepository;

	@Override
	public List<PublisherDTO> getAllPublishers() {
		
		return convertPublishersToListDTO(publisherRepository.findAll());
		
	}

	@Override
	public void createPublisher(PublisherDTO requestPublisher) {
		
		Publisher savedPublisher = publisherRepository.save(super.convertDTOToPublisher(requestPublisher));
		System.out.println(savedPublisher.get_id());
		
	}

	@Override
	public List<PublisherDTO> getPublisherByName(String name) {
		return convertPublishersToListDTO(publisherRepository.findByNameContaining(name)) ;
	}

	@Override
	public List<AuthorDTO> getAllAuthors() {
		return convertAuthorsToListDTO(authorRepository.findAll()); 
	}

	@Override
	public List<AuthorDTO> getAuthorByName(String name) {
		return convertAuthorsToListDTO(authorRepository.findByNameContaining(name));
	}

	@Override
	public void createAuthor(AuthorDTO requestAuthor) {
		
		Author savedAuthor = authorRepository.save(super.convertDTOToAuthor(requestAuthor));
		System.out.println(savedAuthor.get_id());
		
	}

	@Override
	public List<BookDTO> getAllBooks() {
		return convertBooksToListDTO(bookRepository.findAll());
	}

	@Override
	public List<BookDTO> getBookByTitle(String title) {
		return convertBooksToListDTO(bookRepository.findByTitleContaining(title));
	}

	@Override
	public void createBook(BookDTO requestBook) {
		
		Book book = super.convertDTOToBook(requestBook);
		
		Author author = authorRepository.findByName(requestBook.getAuthor());
		
		Publisher publisher = publisherRepository.findByName(requestBook.getPublisher());
		
		if (author==null)
			throw new NoDataFoundException("El autor no está registrado");
		
		if (publisher!=null) {
			
			if(publisher.getMaxBookCount()!=-1) {
				
				if(publisher.getBookCount()+1>publisher.getMaxBookCount()) {
					throw new FailedOperationException("No es posible registrar el libro, se alcanzó el máximo permitido.");
				}
				
				
			} 
			
			publisher.setBookCount(publisher.getBookCount()+1);
			publisherRepository.save(publisher);
			
			
		}else {
			throw new NoDataFoundException("El autor no está registrado");
		}
		
			
		
		book.setAuthor(author);
		book.setPublisher(publisher);
		
		Book savedBook = bookRepository.save(book);
		System.out.println(savedBook.get_id());
		
	}

}

package com.nexos.demo.service;

import java.util.List;

import com.nexos.demo.dto.AuthorDTO;
import com.nexos.demo.dto.BookDTO;
import com.nexos.demo.dto.PublisherDTO;


public interface LibraryService {
	
	public List<PublisherDTO> getAllPublishers();
	
	public List<PublisherDTO> getPublisherByName(String name);
	
	public void createPublisher(PublisherDTO publisher);
	
	public List<AuthorDTO> getAllAuthors();
	
	public List<AuthorDTO> getAuthorByName(String name);
	
	public void createAuthor(AuthorDTO requestAuthor);
	
	public List<BookDTO> getAllBooks();
	
	public List<BookDTO> getBookByTitle(String title);
	
	public void createBook(BookDTO requestBook);
	
}

package com.nexos.demo.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class NoDataFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1151651553680129731L;
    private String message;

    public NoDataFoundException(String message) {

        this.message = message;
    }
}

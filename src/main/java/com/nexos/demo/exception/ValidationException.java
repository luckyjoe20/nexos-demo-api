package com.nexos.demo.exception;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;

import org.springframework.http.HttpStatus;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class ValidationException implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5103144682421103492L;
	private HttpStatus status;
    private String message;
    private List<String> errors;

    public ValidationException(HttpStatus status, String message, List<String> errors) {
        super();
        this.status = status;
        this.message = message;
        this.errors = errors;
    }

    public ValidationException(HttpStatus status, String message, String error) {
        super();
        this.status = status;
        this.message = message;
        errors = Arrays.asList(error);
    }

}

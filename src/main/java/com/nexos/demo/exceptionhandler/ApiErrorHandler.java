package com.nexos.demo.exceptionhandler;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.nexos.demo.exception.FailedOperationException;
import com.nexos.demo.exception.NoDataFoundException;
import com.nexos.demo.exception.ValidationException;

@ControllerAdvice
public class ApiErrorHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException exception,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {


        List<String> errors = new ArrayList<String>();
        for (ObjectError error : exception.getBindingResult().getAllErrors()) {
        	errors.add(error.getDefaultMessage());
        }
        ValidationException validationException = new ValidationException(status, "Error de validación", errors);

        return new ResponseEntity<Object>(validationException, headers, status);
    }
    
    @ExceptionHandler(NoDataFoundException.class)
    public ResponseEntity<Object> handleNodataFoundException(
	        NoDataFoundException ex, 
	        WebRequest request) {

        return new ResponseEntity<>(ex, HttpStatus.NOT_FOUND);
    }
    
    @ExceptionHandler(FailedOperationException.class)
    public ResponseEntity<Object> handleFailedOperationException(
    		FailedOperationException ex, 
	        WebRequest request) {

        return new ResponseEntity<>(ex, HttpStatus.BAD_REQUEST);
    }

}
